/**
 * this code is annoying to make, please keep it reusable
 */
Dragaround=function(domEl){
    var handles=new Set();
    var Handle=function(props){
        var self=this;
        var myEl=document.createElement("div");
        myEl.className="dragaround-handle";
        var position={x:0,y:0};
        var clickPosition={x:0,y:0};
        domEl.appendChild(myEl);
        if(props.x!==undefined) position.x=props.x;
        if(props.y!==undefined) position.y=props.y;
        if(props.width!==undefined) myEl.style.width=props.width+"px";
        if(props.height!==undefined) myEl.style.height=props.height+"px";
        updatePosition();

        beingDragged=false;

        var public=this.public={};
        public.onDragStart=function(pos){}
        public.onDragEnd=function(pos){}
        public.onDrag=function(pos){}
        public.reposition=function(newPos){
            position.x=newPos.x;
            position.y=newPos.y;
            updatePosition();
        }

        function updatePosition(){
            myEl.style.left=position.x+"px";
            myEl.style.top=position.y+"px";
        }
        this.dragStartHandler=function(){
            mouse.isDragging.add(self);
            clickPosition={
                x:position.x-mouse.x,
                y:position.y-mouse.y,
            }
            public.onDragStart(position);
        }
        this.dragEndHandler=function(){
            mouse.isDragging.delete(this);
            position={
                x:mouse.x+clickPosition.x,
                y:mouse.y+clickPosition.y,
            }
            updatePosition();
            public.onDragEnd(position);
        }
        this.dragHandler=function(){
            position={
                x:mouse.x+clickPosition.x,
                y:mouse.y+clickPosition.y,
            }
            updatePosition();
            public.onDrag(position);
        }
        
        myEl.addEventListener("mousedown", this.dragStartHandler);
        
        this.remove=function(){
            myEl.remove();
            handles.delete(self);
        }
    }

    this.createHandle=function(props){
        var newHandle=new Handle(props);
        handles.add(newHandle);
        return newHandle.public;
    }
    var mouse={
        x:0,y:0,
        leftButtonPressed:false,
        rightButtonPressed:false,
        isDragging:new Set(),
    }
    function dragStart(evt){
        evt.preventDefault();
    }
    window.addEventListener('DOMContentLoaded', () => {
        domEl.addEventListener("mousedown", dragStart);
        domEl.addEventListener("mouseup", function(evt){
            evt.preventDefault();
            mouse.isDragging.forEach(function(handle){
                handle.dragEndHandler();
            });
            mouse.isDragging.clear();
        });
        domEl.addEventListener("mousemove",function(evt){
            mouse.x=event.clientX;
            mouse.y=event.clientY;
        
            evt.preventDefault();
            mouse.isDragging.forEach(function(handle){
                handle.dragHandler();
            });
        
        });

    });
}